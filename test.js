const { factorial, addition, result, div_check } = require('../src/util.js');
const { expect, assert } = require('chai');


// A test case or a unit test is a single description about the desired behavior of a code either passes or fails
// Test Suites are made up of collection of test cases that should be executed together. The 'describe' keyword is used to group tests together.

describe('test_fun_factorials', () => {
	// In mocha, the describe() function is used to group tests. It accepts a string to "describe the group of test".
	it('test_fun_factorial_5!_is_120', () => {
		// In mocha, the it function is used to execute individual tests. It accepts a string to describe the test and a callback function to execute assertions.
		// A callback fucntion is used to execute individual callback function to execute assertiobns.
		// A callback fucntion is a function passed into a nother function as an argument
		// A callback function is invoked after the first function is completed
		const product = factorial(5);
		expect(product).to.equal(120);
	});

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);		
	});
});


describe('test_divisibility_by_5_or_7', () => {
	it('test_100_is_divisible_by_5', () => {
		assert.isTrue(div_check(100), 'must be true')
		// expect(div_check(100)).to.equal(true);	
	});

	it('test_49_is_divisible_by_7', () => {
		assert.isTrue(div_check(49), 'must be true')
		// expect(div_check(100)).to.equal(true);	
	});

	it('test_30_is_divisible_by_5', () => {
		assert.isTrue(div_check(30), 'must be true')
		// expect(div_check(100)).to.equal(true);	
	});

	it('test_56_is_divisible_by_7', () => {
		assert.isTrue(div_check(56), 'must be true')
		// expect(div_check(100)).to.equal(true);	
	});
})

// There are three types of assertion styles
/*

	TDD Assertion Style: "assert"
	- TDD or test-driven development  is a software development relying on software requirements being converted to test cases before software is fully deveoped. In TDD, we create the test before the actual code.
	- uses "programming language"
	- from "developer's point of view" and focuses on the implementation of the unit/class/feature.

	BDD Assertion Style: "expect", "should"
	- BDD or behavior-driven development is a method in which an application is documented and designed around the behavior a user expects to experience when interacting with it.
	- uses "natural language" to specify its test.
	- from "customer's point of view" and focuses on expected behavior of the whole system.
	- The biggest contribution of BDD over TDD is making non-technical people (product owners/customers) a part of the software development process at all levels as writing executable scenarios in natural languages.
	- BDD is TDD with different words. User friendly words/Non-tech people friendly.

	**assert and expect are the most commonly used

*/

// ASSERT VS. EXPECT
/*

	Both assert and expect can be used in testing but there are slight differences.

*/


// ASSERT
/*

	-Assert  uses the assert-dot notation that node.js has. The assert module provides several additional tests.
	- In all cases, the assert style also allows you to include an optional message in the assert statement.
	- Assert is not chainable.

*/
describe('This is an assert example on addition', function () {
	it('should be able to test addition', function () {
		assert.equal(addition(2,4), 6);
	})
});


// EXPECT
// The expect module uses unchainable language to construct assertions.
describe('This is an expected example on addition', function () {
	it('should be able to test addition too', function () {
		expect(addition(8,8)).to.equal(16);
		expect(addition(8,1)).to.equal(9);
	})
});


// SOME OF THE ASSERT FUNCTION
/*

	.equal
	- Asserts non-strict equality (==) of actual and expected value.
	Syntax:
		assert.equal(actual, expected, ['message']);

*/
describe('this is an example of an equal function', function () {
	it('should run', function () {
		assert.equal(3, '3', '== coerces values to strings');
		assert.equal(addition(1,2), '3', '== coerces values to strings');
	})
});

/*

	.deepEqual
	- Asserts that actual is "deeply" equal to expected.
	Syntax:
		assert.deepEqual(actual, expected, ['message']);

*/
describe('An example of a deepEqual', function () {
	it('should be deepEqual', function () {
		// A string cannot be equal to number
		assert.deepEqual(3, 3, 'They should be deep equal')
	})
});

/*
	.isTrue
	- Asserts that a value is true
	Syntax:
		assert.isTrue(value, ['message']);

*/

describe('this is an example of the isTrue', function () {
	it('should be true', function () {
		const teaServed = true;
		assert.isTrue(teaServed, 'kahit ano');
	});
	it('should be true', function () {
		assert.isTrue(result(), 'kahit ano');
	});
});

// EXPERIMENT ONLY
describe('Collection of asserts', function () {
	it('is a collection of assert', function () {
		assert.equal(3, '3', '== coerces values to strings');
		assert.deepEqual(3, 3, 'They should be deep equal');
		assert.isTrue(result(), 'kahit ano');
	}); // not a best practice to have multiple asserts in one it.
		// one test per unit only
});

// SOME OF THE EXPECT FUNCTIONS

/*
	.equal
	- Asserts that the target is strictly equal to the given value.
	Syntax:
		expect(target).to.equal(value);
*/
describe('this is an example of an expect equal', function () {
	it('should be equal', function () {
		expect(1).to.equal(1);
	});
});

/*
	.true
	- Asserts that the target is strictly (===) equal to true
	Syntax:
		expect(target).to.be.true;
*/
describe('this should be true', function () {
	it('should be true', function () {
		let stand = true;
		expect(stand).to.be.true;
	});
});